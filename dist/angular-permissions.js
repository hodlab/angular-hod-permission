angular.module('templates-angularpermissions', ['roles.html']);

angular.module("roles.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("roles.html",
		"<div class=\"mdl-grid mdl-grid--no-spacing\">" +
		"<div class=\"mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--12-col-phone no-p-l\">" +
		"<div class=\"p-20 ml-card-holder ml-card-holder\">" +
		"<div class=\"mdl-card mdl-shadow--1dp\">" +
		"<div class=\"mdl-card__title\">" +
		"<h2 class=\"mdl-card__title-text\">Listagem de Papeis</h2>" +
		"</div>" +
		"<div class=\"mdl-layout__content\">" +
		"<div class=\"p-20\" ng-if=\"roles.length === 0 || !roles\">Nenhum papel adicionado</div>" +
		"<table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp w-100\" ng-if=\"roles.length > 0\">" +
		"<thead>" +
		"<tr>" +
		"<th class=\"mdl-data-table__cell--non-numeric\">Papel</th>" +
		"<th>Permissões</th>" +
		"<th></th>" +
		"</tr>" +
		"</thead>" +
		"<tbody>" +
		"<tr ng-repeat=\"role in roles\">" +
		"<td class=\"mdl-data-table__cell--non-numeric p-b-18\">" +
		"<div class=\"f16 f-r\">" +
		"{{role.name}}" +
		"</div>" +
		"</td>" +
		"<td>" +
		"<ul class=\"demo-list-item mdl-list overflowPermission\">" +
		"<li class=\"mdl-list__item\" ng-repeat=\"(key, value) in role.permissions\">" +
		"<span class=\"mdl-list__item-primary-content\">" +
		"{{value}}" +
		"</span>" +
		"</li>" +
		"</ul>" +
		"</td>" +
		"<td>" +
		"<button class=\"mdl-button mdl-js-button mdl-button--raised mdl-button--colored m-r-20\" ng-click=\"editRole(role)\">" +
		"Editar {{role.name}}" +
		"</button>" +
		"</td>" +
		"</tr>" +
		"</tbody>" +
		"</table>" +
		"</div>" +
		"</div>" +
		"<div class=\"mdl-card mdl-shadow--1dp\">" +
		"<div class=\"mdl-card__title\">" +
		"<h2 class=\"mdl-card__title-text\">Adicionar Papel</h2>" +
		"<div class=\"m-l-auto f-right\">" +
		"<button ng-if=\"action == 'add'\" class=\"mdl-button mdl-js-button mdl-button--raised mdl-button--colored m-r-20\" ng-click=\"sendAdd()\" ng-disabled=\"!tmp.name || !tmp.description || tmp.permissions.length === 0\">" +
		"Enviar Papel" +
		"</button>" +
		"<button ng-if=\"action == 'edit'\" class=\"mdl-button mdl-js-button mdl-button--raised mdl-button--colored m-r-20\" ng-click=\"sendEdit()\" ng-disabled=\"!tmp.name || !tmp.description || tmp.permissions.length === 0\">" +
		"Editar Papel" +
		"</button>" +
		"</div>  " +
		"</div>" +
		"<div class=\"mdl-layout__content\">" +
		"<div class=\"mdl-grid p-20\">" +
		"<div class=\"mdl-cell mdl-cell--6-col\">" +
		"<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">" +
		"<input class=\"mdl-textfield__input\" type=\"text\" id=\"sample3\" ng-disabled=\"action == 'edit'\" ng-model=\"tmp.name\">" +
		"<label class=\"mdl-textfield__label\" for=\"sample3\">Nome do Papel</label>" +
		"</div>" +
		"</div>" +
		"<div class=\"mdl-cell mdl-cell--6-col\">" +
		"<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">" +
		"<input class=\"mdl-textfield__input\" type=\"text\" id=\"sample3\"  ng-model=\"tmp.description\">" +
		"<label class=\"mdl-textfield__label\" for=\"sample3\">Descrição do Papel</label>" +
		"</div>" +
		"</div>" +
		"</div>" +
		"<div class=\"mdl-grid mdl-grid--no-spacing\">" +
		"<div class=\"p-20\" ng-if=\"permissions.length === 0 || !permissions\">Nenhuma permissão adicionada.</div>" +
		"<div class=\"mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-cell--3-col-phone p-20 pad-l-20\" ng-repeat=\"role in permissions\">" +
		"<div class=\"mdl-card mdl-shadow--0dp  mdl-color--cyan\">" +
		"<div class=\"mdl-card__title block\">" +
		"<h4 class=\"mdl-card__title-text mdl-color-text--white f18 w600\">{{role.name}}</h4>" +
		"</div>" +
		"<div class=\"p-15 color-font\">" +
		"<md-list ng-cloak>" +
		"<md-list-item ng-repeat=\"permission in role.permissions\">" +
		"<p>{{permission.name}}</p>" +
		"<md-checkbox class=\"md-secondary\" checklist-model=\"tmp.permissions\" checklist-value=\"permission.value\"></md-checkbox>" +
		"</md-list-item>" +
		"</md-list>" +
		"</div>    " +
		"</div>" +
		"</div>" +
		"</div>" +
		"</div>" +
		"</div>" +
		"</div>" +
		"</div>" +
		"</div>" + ""
		);
}]);

angular.module('hod-permissions', ['templates-angularpermissions']);

angular.module('hod-permissions').factory('hodPermissionsSettings', function(RolesHandler, PermissionsHelper, $rootScope, $mdToast){

	function popup(){
		var msg = settings.msgContent || 'Você não é autorizado para acessar essa página';
		var position = settings.msgPosition || 'top right';
		$mdToast.show(
      		$mdToast.simple()
		        .textContent(msg)
		        .position(position)
		        .hideDelay(3000)
	    );
	}

	$rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
		var check = function(){
			var hasPermission = PermissionsHelper.hasAccess(toState.role);
			if (hasPermission) return;
			event.preventDefault();
			popup();
		}
		if(!toState.role) return;
		check();
	});

	var settings = {};

	settings.setSettings = function(){
		var configs = {};
		configs.msgUserBlocked = function(){
			var msgs = {};
			msgs.msg = function(msg){
				settings.msgContent = msg;
			}
			msgs.position = function(position){
				settings.msgPosition = position;
			}
			return msgs;
		}
		configs.setRoleUrl = function(roleUrl){
			settings.urlRole = roleUrl;
			configs.verify();
		}
		configs.setUrlBase = function(url){
			settings.urlBase = url;
			configs.verify();
		}
		configs.setPermission = function(permissions){
			settings.permissions = permissions;
			configs.verify();
		}
		configs.verify = function(){
			if(settings.urlRole && settings.urlBase){		
				RolesHandler.getAllRoles().then(function(data){

				})
			}
			if(!settings.permissions){
				settings.permissions = [];
			}
		}
		return configs;
	}
	return settings;
});

angular.module('hod-permissions').directive('hodPermissions', function(hodPermissionsSettings){
	return {
		restrict: 'EA',
        replace: true,
        transclude: true,
		templateUrl: function(element, attributes) {
            return attributes.template || 'roles.html';
        },
        controller: ['hodPermissionsSettings', '$scope', '$rootScope', 'RolesHandler','PermissionsHelper',function(hodPermissionsSettings, $scope, $rootScope, RolesHandler, PermissionsHelper){
        	console.log('hodPermissions loaded')
        	$scope.action = "add";
        	$scope.permissions = hodPermissionsSettings.permissions;
        	$scope.roles = RolesHandler.allRoles;
        	$scope.tmp = {
        		permissions: [],
        		name: null,
        		description: null
        	};
        	$scope.$watch('permissions', function(permission) {
        		try{
        			if(typeof permission != 'object'){
        				throw 'Your scope directive permissions is array?'
        				return;
        			}
    				$scope.permissions = permission;
        		}catch(err){
        			throw 'Your scope directive permissions is array?'
        			return;
        		}
        	});

        	function setDefaultTmp(){
        		$scope.tmp = {
	        		permissions: [],
	        		name: null,
	        		description: null
        		};
        	}

        	$scope.editRole = function(role){
        		$scope.action = "edit";
        		$scope.tmp = role;
        	}

        	$scope.sendAdd = function(){
        		RolesHandler.addRole($scope.tmp).then(function(data){
        			hodPermissionsSettings.verify();
        			setDefaultTmp();
        			console.log(data);
        		})
        	}

        	$scope.sendEdit = function(){
        		RolesHandler.editRole($scope.tmp).then(function(data){
        			hodPermissionsSettings.verify();
        			setDefaultTmp();
        			$scope.action = "add";
        			console.log(data);
        		})
        	}
        }]
	}
});

angular.module('hod-permissions').directive('checkPermissions', function(PermissionsHelper, $rootScope, $timeout, $compile) {
	return {
	    restrict: 'A',
        link: function(scope, elem, attrs, ctrl) {
        	elem.hide();
    		$rootScope.$watch('hodRoles', function(){
        		console.log('directiveRunned');
	            if(!attrs.checkPermissions) return;
                var hasPermission = PermissionsHelper.hasAccess(attrs.checkPermissions);
                if (hasPermission) elem.show();
    		})
    	}
	};
});

angular.module('hod-permissions').directive('checkPermissionsMultiple', function(PermissionsHelper, $rootScope, $timeout) {
	return {
	    restrict: 'A',
        link: function(scope, elem, attrs, ctrl) {
        	elem.hide();
    		$rootScope.$watch('hodRoles', function(){
        		console.log('directiveRunnedMultiple');
	            if(!attrs.checkPermissionsMultiple) return;
	            var attrs = attrs.checkPermissionsMultiple;
                var hasPermission = PermissionsHelper.hasAccess(attrs.split(','));
                if (!hasPermission) elem.show();
    		})
    	}
	};
});

angular.module('hod-permissions').factory('PermissionsHelper', function(RolesHandler, hodSecurePermissions){
	var helper = {};

	helper.userAccess = null; //crypted
	helper.userPermissions = null;

	helper.setRole = function(role){
		window.localStorage.setItem('roleHashed', hodSecurePermissions.set(role));
	}

	helper.getRole = function(){
		if(window.localStorage.getItem('roleHashed')){
			helper.userAccess = window.localStorage.getItem('roleHashed');
		}else if(window.localStorage.getItem('role')){
			helper.userAccess = hodSecurePermissions.set(window.localStorage.getItem('role'));
		}
		return helper.userAccess;
	}

	helper.setUserPermissions = function(){
		if(!helper.getRole()){
			window.console.warn("HOD Permissions: Verify your key role on localStorage or setRole function");
			return;
		}
		var userAccessDecrypted = hodSecurePermissions.get(helper.userAccess);
		helper.userPermissions = RolesHandler.allRoles.filter(function(permission){
			return(permission.name.toUpperCase() == userAccessDecrypted.toUpperCase())
		});
		if(helper.userPermissions.length > 0){
			helper.userPermissions = helper.userPermissions[0].permissions
		}else{
			helper.userPermissions = undefined;
		}
	}

	helper.hasAccess = function(permission){
		if(typeof permission === 'string'){
			permission = [permission];
		}
		function containsAll(needles, haystack){ 
			for(var i = 0 , len = needles.length; i < len; i++){
				if($.inArray(needles[i], haystack) == -1) return false;
			}
			return true;
		}
		var grant = false;
		var init = function(){
			if(!helper.userPermissions){
				return;
			}
			var contains = containsAll(permission, helper.userPermissions);
			if(contains) grant = true;
		}
		helper.setUserPermissions();
		init();
		
		return (grant === true) ? true : null;
	}

	return helper;
});

angular.module('hod-permissions').factory('RolesHandler', function($q, $http, $injector, $rootScope){
	var service = {};

	service.allRoles = [];

	service.getAllRoles = function(){
		var def = $q.defer();
		var hodPermissionsSettings = $injector.get('hodPermissionsSettings')
		$http.get(hodPermissionsSettings.urlBase + '/' + hodPermissionsSettings.urlRole).then(function(permissions){
			$rootScope.hodRoles = permissions.data;
			service.allRoles = permissions.data;
			def.resolve(permissions.data);
		}).catch(function(err){
			def.reject('Problem to get Permissions');
			throw new TypeError('There was an error permissions.');
		});
		return def.promise;
	}

	service.addRole = function(role){
		var def = $q.defer();
		var hodPermissionsSettings = $injector.get('hodPermissionsSettings')
		$http.post(hodPermissionsSettings.urlBase + "/" + hodPermissionsSettings.urlRole, role).then(function(permissions){
			service.getAllRoles();
			def.resolve(permissions.data);
		}).catch(function(err){
			def.reject('Problem to add Permission');
			throw new TypeError("There was an error adding.");
		})
		return def.promise;
	}

	service.editRole = function(role){
		var def = $q.defer();
		var hodPermissionsSettings = $injector.get('hodPermissionsSettings')
		if (role.changeDateTime) delete role.changeDateTime;
		if (role.createDateTime) delete role.createDateTime;

		$http.put(hodPermissionsSettings.urlBase + '/' + hodPermissionsSettings.urlRole + '/' + role._id, role).then(function(permissions) {
			service.getAllRoles();
			def.resolve(permissions.data);
		}).catch(function(err){
			def.reject('Problem to edit Permission');
			throw new TypeError("There was an error edit.");
		});
		return def.promise;
	}

	return service;
})

angular.module('hod-permissions').factory('hodSecurePermissions', function(){
	var crypt = {};

	crypt.spass = "8a7e0d6f726d7d7d7286a002011b279e";
	crypt.SecureRole = "fa77f39565ed4fda995af4f21714d527";

	crypt.set = function(content){
		return CryptoJS.AES.encrypt(content, crypt.spass);
	}
	crypt.get = function(content){
		return CryptoJS.AES.decrypt(content, crypt.spass).toString(CryptoJS.enc.Utf8);
	}

	return crypt;
});