#** Angular-hod-permission **#

Is a component that will create a authorization and Role based permissions, created mainly for systems HodLab Creative.


# How do I add this to my project? #
---
**Install:**

* bower install angular-hod-permission --save
* bower install crypto-js --save
* bower install angular-material --save

```
#!html
<script src="angular-hod-permission/dist/angular-permissions.js"></script>
<script src="crypto-js/crypto-js.js"></script>
<script src="angular-material/angular-material.min.js"></script>
```

# Configure #
---
** Step One - *Required* **

In your app.js, inject 'hodPermissionsSettings' in App.run function and add this lines below in this file too:

```
#!javascript

hodPermissionsSettings.setSettings().setUrlBase('http://apiconn.hodlab.com.br');
hodPermissionsSettings.setSettings().setRoleUrl('roles');
hodPermissionsSettings.setSettings().setPermission(permissions);
```

* ** setUrlBase(string): ** This set url for get all, edit and add json arrays permissions by API. 
* ** setRoleUrl(string): ** This set end-point used for get all, edit and add roles.
* ** setPermission(array): ** This set array for load dynamic permissions.

```
#!javascript

// Example for setPermission(array) function
     var array =
     [
          {
               "name": "Usuário",
               "permissions":
               [
                    {
                         "name": "Adicionar Usuário no Sistema"
                         "value": "add_user"
                    },
                    {
                         "name": "Editar Usuário no Sistema"
                         "value": "edit_user"
                    }
               ]
          }
     ]
```

Optional Settings

* ** hodPermissionsSettings.setSettings().msgUserBlocked().msg(string): ** Set custom message to error block route.

* ** hodPermissionsSettings.setSettings().msgUserBlocked().position(string): ** Set custom position for message ($toast of Angular Material). *Ex: 'top right', 'top left'.*


** Step Two - *Required* **

In app.js into App.config, add role key permission on route to block page access.

```
#!javascript

// Example Block Route
     $stateProvider
        .state('feeds', {
        url: '/feeds',
        role:"feeds_list"
        requiredLogin:false,
        abstract: true,
        templateUrl: 'templates/feeds.html'
     }) 
```

# Dependencies #
---
* Angular
* Crypto-JS
* Angular Material


# Directives #
---

## hod-permissions ##

**About**

This directive is used to show the table of adding and editing permissions/roles.

**Usage**
```
#!html
<hod-permissions template="templates/partials/{{your_template_html}}"></hod-permissions>
```
* **template:** Path to a custom template

## check-permissions ##

**About**

This directive restrict element by permission name (ex: user_add, user_edit, finance_add).

**Usage**
```
#!html
<!-- For Element Example -->
<div class="mdl-grid">
     <div class="mdl-cell mdl-cell--12-col">
          <span check-permissions="add_user">Add User on System</span>
     </div>
</div>

<!-- For Menu Example-->
<ul ml-menu close-others="false" class="demo-navigation mdl-navigation">
     <ml-menu-item check-permissions="list_user">
          <a class="mdl-navigation__link" href="#/app/user"><i class="mdl-color-text--blue-grey-400 material-icons">person</i>Users</a>
     </ml-menu-item>
</ul>
```

## check-permissions-multiple ##

**About**

This directive restrict element by multiple permissions name separated by commas (ex: 'user_add,user_edit, finance_add').

*ps: the element only remove if all permissions are granted for user*

**Usage**
```
#!html
<!-- For Element Example -->
<div class="mdl-grid">
     <div class="mdl-cell mdl-cell--12-col">
          <span check-permissions-multiple="list_user,add_user">Add User on System</span>
     </div>
</div>

<!-- For Menu Example-->
<ul ml-menu close-others="false" class="demo-navigation mdl-navigation">
     <ml-menu-item check-permissions-multiple="list_user,add_user">
          <a class="mdl-navigation__link" href="#/app/user"><i class="mdl-color-text--blue-grey-400 material-icons">person</i>Users</a>
     </ml-menu-item>
</ul>
```

# Services #
---

## RolesHandler##
Inject 'RolesHandler' on your controller.

* ** allRoles: ** returns a array containing all roles.

* ** getAllRoles(): ** update allRoles.

* ** addRole(role): ** this function is used for add role on system.

* ** editRole(role): ** this function is used for edit role on system.

** role: ** is object following structure below:

```
#!javascript
{
     "name":"ADMIN",
     "permissions":['add_user','edit_user','delete_user']
}
```


# Starter Guide #
---

## First example ##
----

After install angular-hod-permission, follow the example below.

We first have to do this:

```
#!javascript
App.module('App', ['hod-permissions']);

```